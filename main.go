package main

import (
	"context"
	"fmt"
	"time"
)

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	nums := make(chan int)
	go func() {
		nums <- expensiveWorker()
	}()

	select {
	case num := <-nums:
		fmt.Printf("Got number! %d", num)
	case <-ctx.Done():
		fmt.Print("Timed out!")
	}
}

func expensiveWorker() int {
	time.Sleep(5 * time.Second)

	return 10
}
